from .app import manager, db

@manager.command
def loaddb(filename):
	'''Creates the tables and populates them with data.'''
	
	# création de toutes les tables
	db.create_all()
	
	# chargement de notre jeu de données
	import yaml
	wines = yaml.load(open(filename))
	
	# import des modèles
	from .models import Wine
	
	# Creation de la table Wine
	for b in wines:
		o = Wine(category   = b["category"] ,
				 country    = b["country"] ,
				 image	    = b["image"] ,
				 name	    = b["name"] ,
				 quantity	= b["quantity"] ,
				 region	    = b["region"] ,
				 varietal	= b["varietal"] ,
				 vintage	= b["vintage"]) 
		db.session.add(o)
	db.session.commit()

@manager.command
def syncdb():
	'''Creates all missing tables.'''
	db.create_all()

@manager.command
def newuser(username, passwd):
        '''Adds a new user.'''
        from .models import User
        from hashlib import sha256
        m = sha256()
        m.update(passwd.encode())
        u = User(username=username, password=m.hexdigest())
        db.session.add(u)
        db.session.commit()
