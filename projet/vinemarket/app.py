#! /usr/bin/env python3
from flask import Flask
from flask.ext.bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)
app.debug = True
app.config['SECRET_KEY'] = "594f6cc4-014a-4aaa-98c7-26e799292910"

from flask.ext.script import Manager
manager = Manager(app)


import os.path
def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))
		
from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../vinemarket.db'))
db = SQLAlchemy(app)

from flask.ext.login import LoginManager
login_manager = LoginManager(app)
login_manager.login_view = "login"
